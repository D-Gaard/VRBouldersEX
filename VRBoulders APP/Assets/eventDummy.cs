using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eventDummy : MonoBehaviour
{
  public Material m;
  public Renderer r;
  private float t = 0.0f;
  private bool first = true;
  // Start is called before the first frame update
  void Start()
  {
    EventManager.current.onDummyEvent += dumby;
  }

  void Update()
  {
    if (first)
    {
      first = false;
      EventManager.current.ClimbStart();

    }
    
    t += Time.deltaTime;
    if (t > 5.0f)
    {
      EventManager.current.ClimbEnd();

    }
    else
    {

      EventManager.current.ClimbAttempt("hejsa");
    }
  }

  // Update is called once per frame
  private void dumby()
  {
    r.material = m;
  }
}
