using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxColision : MonoBehaviour
{
    public float magnitude = 20;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
/*
    void OnCollisionEnter(Collision o){
        if (o.gameObject.tag == "GameController"){
            Vector3 dir = o.contacts[0].point -transform.position;

            dir = -dir.normalized;

            GetComponent<Rigidbody>().AddForce(dir*Force);
        }
    }
*/

     void OnTriggerEnter (Collider other)
     {
         var force = transform.position - other.transform.position;
 
         force.Normalize ();
         GetComponent<Rigidbody>().AddForce (force * magnitude);
 
 
     }
}
