using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class ButtonActionHandler : MonoBehaviour
{
  public UnityEvent Action;
  public Image _image;
  private TMP_Text t;

  void Start(){
    t = gameObject.GetComponent<TMP_Text>();
  }


  void OnTriggerEnter(Collider c){
    //change background
    if (c.tag == "MenuControler"){
      _image.color = Color.red;
    }

  }
  void OnTriggerStay(Collider c)
  {
    if (c.tag == "MenuControler")
    {
      if (OVRInput.GetDown(OVRInput.RawButton.X)){
        Action.Invoke();
        t.color = Color.blue;
      }
    }
  }

  void OnTriggerExit(Collider c){
    //return to normal background
    if (c.tag == "MenuControler") {
      _image.color = Color.black;
    }
  }
}
