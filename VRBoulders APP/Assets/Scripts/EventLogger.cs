using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Linq;
using System.IO;
using OVRTouchSample;

public class EventLogger : MonoBehaviour
{
  public string filename = "";
  public string logtype = "ClimbAttempt";
  public static int userID = 1;
  public static int trialID = 1;

  private bool isLogging = false;
  private bool isWriting = false;
  private List<string> loggedRows;
  private float time;

  void Start(){
    EventManager.current.onClimbAttempt += OnClimbAttempt;
    EventManager.current.onClimbStart += OnClimbStart;
    EventManager.current.onClimbEnd += OnClimbEnd;
  }

  void LogToCSV()
  {
    string header = "userID;trialID;time;logtype;string";
    string filepath = Application.persistentDataPath + "/" + filename + logtype + System.DateTime.Now.ToString("ddmmyyyy-HHmmss") + ".csv";
    using (StreamWriter sw = new StreamWriter(filepath))
    {
      sw.WriteLine(header);
      foreach (string s in loggedRows)
      {
        sw.WriteLine(s);
      }
    }
  }

  void StartLogging()
  {
    if (isLogging == false && isWriting == false)
    {
      isLogging = true;
      loggedRows = new List<string>();
    }
  }

  void StopLogging()
  {
    if (isLogging)
    {
      isWriting = true;
      LogToCSV();
      isWriting = false;
      isLogging = false;
    }
  }
  void Log(int userId, int trialId, long timestamp, string eventName, string items)
  {
    string tempString = "" + userId + ";" + trialId + ";" + timestamp + ";" + eventName + ";" + items;
    loggedRows.Add(tempString);
  }


  private void OnClimbAttempt(string s)
  {
    if (isLogging){
      long time = DateTimeOffset.Now.ToUnixTimeSeconds();
      Log(userID, trialID, time, logtype, s);
    }
  }
  private void OnClimbStart(){
    StartLogging();
  }

  private void OnClimbEnd(){
    StopLogging();
  }

}

