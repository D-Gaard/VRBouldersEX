using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Linq;
using System.IO;
using OVRTouchSample;

public class Logging : MonoBehaviour
{
  public string filename = "";
  public string logtype = "Position";
  public Transform target;
  public int userID = 1;
  public int trialID = 1;

  private bool isLogging = false;
  private bool isWriting = false;
  private List<string> loggedRows;
  private float time;

  // Start is called before the first frame update
  void Start()
  {
    //time = Time.time;
    //StartLogging();
  }

  // Update is called once per frame
  void Update()
  {
    if (OVRInput.GetDown(OVRInput.Button.One))
    {
      if(isLogging){
        StopLogging();
      } else {
        StartLogging();
      }
    }
    if (isLogging)
    {
      long time = DateTimeOffset.Now.ToUnixTimeSeconds();

      Log(userID, trialID, time, logtype, (target.position.x.ToString() + ";" + target.position.y.ToString() + ";" + target.position.z.ToString()));
    }

    //if time over
    //if (time + 10.0f < Time.time)
    //{
    //  StopLogging();
    //}
  }

  void LogToCSV()
  {
    string header = "UserID;TrialID;time;logtype;x;y;z";
    string filepath = Application.persistentDataPath + "/" + filename + logtype + System.DateTime.Now.ToString("ddmmyyyy-HHmmss") + ".csv";
    using (StreamWriter sw = new StreamWriter(filepath))
    {
      sw.WriteLine(header);
      foreach (string s in loggedRows)
      {
        sw.WriteLine(s);
      }
    }
  }

  void StartLogging()
  {
    if (isLogging == false && isWriting == false)
    {
      isLogging = true;
      loggedRows = new List<string>();
    }
  }

  void StopLogging()
  {
    if (isLogging)
    {
      isWriting = true;
      LogToCSV();
      isWriting = false;
      isLogging = false;
    }
  }
  void Log(int userId, int trialId, long timestamp, string eventName, string items)//params string[] items)
  {
    string tempString = "" + userId + ";" + trialId + ";" + timestamp + ";" + eventName + ";" + items;
    loggedRows.Add(tempString);
  }

}

