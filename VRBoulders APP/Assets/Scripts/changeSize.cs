using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVRTouchSample;

public class changeSize : MonoBehaviour
{
  public float Scale;
  public float min_scale;
  public float max_scale;
  Vector3 temp;
  // Start is called before the first frame update
  void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    if (OVRInput.GetDown(OVRInput.Button.One))
    {
      //Application.LoadLevel(Application.loadedLevel);
      temp = transform.localScale;

      temp.x += Scale;
      temp.z += Scale;

      if (temp.x > max_scale)
      {
        //we do not want to scale our object above max
        temp.x = max_scale;
        temp.z = max_scale;
      }
      transform.localScale = temp;
    } else if (OVRInput.GetDown(OVRInput.Button.Two)){
      temp = transform.localScale;
        temp.x -= Scale;
        temp.z -= Scale;
      if (temp.x < min_scale){
        //we do not want to scale our object below min
        temp.x = min_scale;
        temp.z = min_scale;
      }
      transform.localScale = temp;  
    }
  }
}
