using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class LongButton : MonoBehaviour
{
    private bool ButtonDown;
    private float ButtonDownTime;
    public float requiredDownTime;

    public UnityEvent Action;

    public Image FillImage;

    void OnCollisionEnter(Collision collision)
    {
        ButtonDown = true;
    }

    void OnCollisionExit(Collision collision)
    {
        ButtonDown = false;
    }


    private void Update()
    {
        if (ButtonDown)
        {
            ButtonDownTime += Time.deltaTime;
            if (ButtonDownTime >= requiredDownTime)
            {
                if (Action != null)
                {
                    Action.Invoke();
                }
                Reset();
            }
            FillImage.fillAmount = ButtonDownTime / requiredDownTime;
        }
        else
        {

            Reset();
        }
    }

    private void Reset()
    {
        ButtonDown = false;
        ButtonDownTime = 0;
        FillImage.fillAmount = ButtonDownTime / requiredDownTime;
    }
}
