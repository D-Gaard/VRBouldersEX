using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hand2 : MonoBehaviour
{
  public Climber climber = null;
  public OVRInput.Controller controller = OVRInput.Controller.None;

  public Vector3 Delta { private set; get; } = Vector3.zero;
  private Vector3 lastPosition = Vector3.zero;


  private GameObject currentPoint = null;
  private List<GameObject> contactPoints = new List<GameObject>();
  public SkinnedMeshRenderer meshRenderer;

  //materials for climb points
  public Material standardMat;
  public Material contactMat;

  private void Awake()
  {
  }

  private void Start()
  {
    lastPosition = transform.position;
  }

  private void Update()
  {
    if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, controller))
    {
      GrabPoint();
    }
    if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, controller))
    {
      ReleasePoint();
    }
  }

  private void FixedUpdate()
  {
    lastPosition = transform.position;
  }

  private void LateUpdate()
  {
    Delta = lastPosition - transform.position;
  }

  private void GrabPoint()
  {
    currentPoint = Utility.GetNearest(transform.position, contactPoints);

    if (currentPoint)
    {
      climber.SetHand(this);
      meshRenderer.enabled = false;
      EventManager.current.ClimbAttempt("S");
    }
    else
    {
      EventManager.current.ClimbAttempt("F");
    }
  }

  public void ReleasePoint()
  {
    if (currentPoint)
    {
      climber.Clearhand();
      meshRenderer.enabled = true;
      currentPoint.GetComponent<Renderer>().material = standardMat;
    }

    currentPoint = null;

  }

  private void OnTriggerEnter(Collider other)
  {
    AddPoint(other.gameObject);
    //OnCollision(other);
    if (other.gameObject.tag == "ClimbPoint")
    {
      other.gameObject.GetComponent<Renderer>().material = contactMat;
    }
  }

  private void AddPoint(GameObject newObject)
  {
    //check if tag matches
    if (newObject.CompareTag("ClimbPoint"))
    {
      contactPoints.Add(newObject);
    }
  }

  private void OnTriggerExit(Collider other)
  {
    RemovePoint(other.gameObject);
    //OnCollision(other);
    if (other.gameObject.tag == "ClimbPoint" && meshRenderer.enabled != false)
    {
      other.gameObject.GetComponent<Renderer>().material = standardMat;
    }
  }

  private void OnTriggerStay(Collider other)
  {
    if (other.gameObject.tag == "ClimbPoint")
    {
      other.gameObject.GetComponent<Renderer>().material = contactMat;
    }
  }

  private void RemovePoint(GameObject newObject)
  {
    if (newObject.CompareTag("ClimbPoint"))
    {
      contactPoints.Remove(newObject);
    }

  }

}
