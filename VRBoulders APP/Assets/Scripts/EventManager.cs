using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventManager : MonoBehaviour
{
  public static EventManager current;
  // Start is called before the first frame update
  void Awake()
  {
    current = this;
  }
  public event Action<string> onClimbAttempt;
  public void ClimbAttempt(string s)
  {
    if (onClimbAttempt != null)
    {
      onClimbAttempt(s);
    }
  }

  public event Action onClimbStart;
  public void ClimbStart()
  {
    if (onClimbStart != null)
    {
      onClimbStart();
    }
  }

  public event Action onClimbEnd;
  public void ClimbEnd()
  {
    if (onClimbEnd != null)
    {
      onClimbEnd();
    }
  }

  public event Action onDummyEvent;
  public void DummyEvent()
  {
    if (onDummyEvent != null)
    {
      onDummyEvent();
    }
  }

}
