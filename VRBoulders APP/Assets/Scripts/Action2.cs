using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Action2 : MonoBehaviour
{
  public Vector3 rotation1 = new Vector3(0, 0, 0);
  public Vector3 rotation2 = new Vector3(0, 0, 0);
  public Vector3 translation1 = new Vector3(0, 0, 0);
  public Vector3 translation2 = new Vector3(0, 0, 0);
  public TMP_Text textID;
  public TMP_Text textTrial;
  public Vector3 startPos = new Vector3(0, 0, 0);
  public Transform _player;
  public OVRManager _camera;
  public Transform _playerCam;
  public Transform _RightHand;
  public Transform _LeftHand;

  public void RotationVersion0()
  {
    _camera.headPoseRelativeOffsetRotation = new Vector3(0, 0, 0);
    _camera.headPoseRelativeOffsetTranslation = new Vector3(0, 0, 0);

    //_playerCam.localEulerAngles = new Vector3(0.0f,0.0f,0.0f);
    //_LeftHand.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
    //_RightHand.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);

  }

  public void RotationVersion1()
  {
    _camera.headPoseRelativeOffsetRotation = rotation1;
    _camera.headPoseRelativeOffsetTranslation = translation1;

    //_playerCam.localEulerAngles = new Vector3(-20.0f, 0.0f, 0.0f);
    //_LeftHand.localEulerAngles = new Vector3(20.0f, 0.0f, 0.0f);
    //_RightHand.localEulerAngles = new Vector3(20.0f, 0.0f, 0.0f);
  }

  public void RotationVersion2()
  {
    _camera.headPoseRelativeOffsetRotation = rotation2;
    _camera.headPoseRelativeOffsetTranslation = translation2;
    //_playerCam.localEulerAngles = new Vector3(-40.0f, 0.0f, 0.0f);
    //_LeftHand.localEulerAngles = new Vector3(40.0f, 0.0f, 0.0f);
    //_RightHand.localEulerAngles = new Vector3(40.0f, 0.0f, 0.0f);

  }

  public void SetID(){
    EventLogger.userID += 1;
    textID.text = "ID: " + EventLogger.userID;
  }

  public void SetTrial()
  {
    EventLogger.trialID += 1;
    textTrial.text = "Trial: " + EventLogger.trialID;
  }

  public void StartClimb(){
    GoToStart();
    EventManager.current.ClimbStart();
  }

  public void GoToStart(){
      _player.position = startPos;
  }

  public void StopLog(){
    EventManager.current.ClimbEnd();
  }

  public void ForskydX(){
    _camera.headPoseRelativeOffsetTranslation += new Vector3(0.1f, 0, 0);
  }
  public void ForskydY()
  {
    _camera.headPoseRelativeOffsetTranslation += new Vector3(0, 0.1f, 0);
  }
  public void ForskydZ()
  {
    _camera.headPoseRelativeOffsetTranslation += new Vector3(0, 0, 0.1f);
  }

  public void ForskydXMinus()
  {
    _camera.headPoseRelativeOffsetTranslation -= new Vector3(0.1f, 0, 0);
  }
  public void ForskydYMinus()
  {
    _camera.headPoseRelativeOffsetTranslation -= new Vector3(0, 0.1f, 0);
  }
  public void ForskydZMinus()
  {
    _camera.headPoseRelativeOffsetTranslation -= new Vector3(0, 0, 0.1f);
  }

}
