using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class onHitByThrow : MonoBehaviour
{
  [SerializeField] private Renderer myObj;
  public string objTag;
  public Material NoCol;
  public Material Col;
  public TMP_Text t;
  private int score = 0;

  // Start is called before the first frame update

  // Update is called once per frame
  void Update()
  {

  }

  void OnTriggerEnter(Collider o)
  {
    if (o.gameObject.tag == objTag)
    {
    myObj.material = Col;
    score += 1;
    t.text = "Score: " + score;
    }
  }

  void OnTriggerExit(Collider o)
  {
    if (o.gameObject.tag == objTag)
    {
      myObj.material = NoCol;
    }
  }
}
