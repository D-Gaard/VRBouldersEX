using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTilt : MonoBehaviour
{       
    private float timer = 0.0f;
    public float waitTime = 5.0f;
    public Vector3 rotation = new Vector3(0,0,0);
    public Vector3 _translation = new Vector3(0, 0, 0);
    private bool isPressed = false;
    public OVRManager camera;

    void Update() 
    {
        TriggerButton(); 
        if (timer > waitTime)
        {
            timer = 0.0f;
            camera.headPoseRelativeOffsetRotation += rotation;
            camera.headPoseRelativeOffsetTranslation += _translation;
        }
    }

    public void TriggerButton()
    {
        if(OVRInput.GetDown(OVRInput.RawButton.X))
        {
            isPressed = true;
        }
        if(OVRInput.GetUp(OVRInput.RawButton.X))
        {
            isPressed = false;
            timer = 0.0f;
        }
        if(isPressed)
        {
            timer += Time.deltaTime;
        }
    }

}
