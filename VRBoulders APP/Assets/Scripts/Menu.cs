using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public CanvasGroup menu;
    private bool menuOpen = false;

    void Update()
    {
        OpenMenu();
    }

    private void OpenMenu()
    {
        if(OVRInput.GetDown(OVRInput.RawButton.Y))
        {
            if(menuOpen)
            {
                menu.interactable = false;
                menu.alpha = 0;
                menuOpen = false;
            }
            else
            {
                menu.interactable = true;
                menu.alpha = 1;
                menuOpen = true;
            }
            
        }
    }
    
}
