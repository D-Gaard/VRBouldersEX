using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class picked : MonoBehaviour
{
    public Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = false;
    }
    void OnTriggerEnter(Collider player){
        if(player.gameObject.tag == "Player"){
            this.gameObject.SetActive(false);

        }
    }
}
