using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HandTrackingUI : MonoBehaviour
{
    public OVRHand hand;
    public OVRInputModule InputModule;

    private void Start()
    {
        InputModule.rayTransform = hand.PointerPose;
    }
}
